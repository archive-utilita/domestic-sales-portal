export const state = () => ({
  address: null,
  addressId: null,
  postcode: null,
  esf: false,
  displayAddress: null
})


export const mutations = {
  updateAddress(state, value) {
    state.address = value;
  },
  updateAddressId(state, id) {
    state.addressId = id;
  },
  updatePostcode(state, postcode) {
    state.postcode = postcode;
  },
  updateEsf(state, bool) {
    state.esf = bool;
  },
  updateDisplayAddress(state, value) {
    state.displayAddress = value;
  },
}
